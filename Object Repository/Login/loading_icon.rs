<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>loading_icon</name>
   <tag></tag>
   <elementGuidId>ddf42450-60c0-421f-b44a-f5566dd79d63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'el-loading-mask']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>el-loading-mask</value>
      <webElementGuid>9a74a2d8-377a-4553-b8fa-78d5f21f2095</webElementGuid>
   </webElementProperties>
</WebElementEntity>
