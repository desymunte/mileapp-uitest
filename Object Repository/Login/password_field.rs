<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>password_field</name>
   <tag></tag>
   <elementGuidId>5ffc1c08-1981-46cb-b13d-42f8a2ace81a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>2be73186-28ae-41f1-b50f-ceed1c8dbab1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
