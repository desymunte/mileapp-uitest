<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>eye_icon</name>
   <tag></tag>
   <elementGuidId>7d34d57e-c42e-4ce9-ae76-ae823e83f16c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'el-input__icon el-icon-view el-input__clear']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>el-input__icon el-icon-view el-input__clear</value>
      <webElementGuid>c59ec0bd-6368-4f81-91c6-88ae9267ce3c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
