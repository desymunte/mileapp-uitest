<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>org_name_field</name>
   <tag></tag>
   <elementGuidId>6e97a46a-ecb7-4c37-90bf-82b57f568aaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'organization']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>organization</value>
      <webElementGuid>343c82c5-18ed-44c0-95ec-e82b7d9252f5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
