<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login_button</name>
   <tag></tag>
   <elementGuidId>2e0257b3-87b1-481c-a695-bb49a84d5432</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'el-button button el-button--primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>el-button button el-button--primary</value>
      <webElementGuid>8c05f9a3-67bd-4564-b888-ac839ce4fcb7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
