<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>username_field</name>
   <tag></tag>
   <elementGuidId>e3ba5952-4cba-411c-aaf3-8b2d909f21c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'email or username']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email or username</value>
      <webElementGuid>28f18c10-0db1-4989-a5fb-fc860effc0e1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
