import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.web_url)

WebUI.waitForElementPresent(findTestObject('Login/login_card'), 10)

WebUI.setText(findTestObject('Login/org_name_field'), findTestData('test_data').getValue(2, 1))

WebUI.clickOffset(findTestObject('Login/org_name_field'), 75, 75)

WebUI.waitForElementPresent(findTestObject('Login/loading_icon'), 0)

WebUI.waitForElementNotHasAttribute(findTestObject('Login/username_field'), 'disabled', 10)

WebUI.waitForElementNotHasAttribute(findTestObject('Login/password_field'), 'disabled', 10)

WebUI.waitForElementNotHasAttribute(findTestObject('Login/login_button'), 'disabled', 10)

//WebUI.closeBrowser()
